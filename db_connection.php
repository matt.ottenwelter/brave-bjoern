<?php

opcache_reset() ;

$mysqli = new mysqli("localhost", "root", "root", "bravebjorn");
$mysqli->set_charset("utf8");

$requete = "SELECT * FROM score ORDER BY SCORE DESC";
$resultat = $mysqli->query($requete);

$i= 0;

foreach($resultat as $tables) {
    echo '<li>' . '<mark>' . $tables['NAME'] . '</mark>' .' - ' . '<small>' . $tables['SCORE'] . '</small>' .'</li>';
    $i++;
    include $tables;
    if($i == 10) break;
}

$mysqli->close();