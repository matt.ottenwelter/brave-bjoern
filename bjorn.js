var jumpSound = new Audio('assets/music/jump.mp3');
var deathSound = new Audio('assets/music/death.mp3');
var isPaused = false;
var selectSound = new Audio('assets/music/menu.mp3');
var goldSound = new Audio('assets/music/coin.mp3');


selectSound.volume = 0.3;
goldSound.volume = 0.4;


function playSelect() {
    selectSound.play();
}

function playGoldSound(){
    goldSound.play();
}

function resetCounter() {
    counter = 0;
}

function randomize(){
    return Math.floor(Math.random() * (3 - 1 + 1) + 1)
}

function view() {
    let game = document.getElementById("myGame");
    let score = document.getElementById("scoreBoard");
    let btnscore = document.getElementById("scorebutton");
    let credits = document.getElementById("allCredits");
    let btncredit = document.getElementById("creditbutton");

    if (game.style.display === "none") {
        game.style.display = "block";
        btnscore.textContent = 'SCOREBOARD';
        score.style.display = "none";
        btncredit.style.display = "block";
        btncredit.textContent = 'CREDITS';
        credits.style.display = "none";

    } else {
        game.style.display = "none";
        btnscore.textContent = 'BACK';
        score.style.display = "block";
        btncredit.style.display = "none";
    }
}

function viewCredits() {
    let game = document.getElementById("myGame");
    let score = document.getElementById("scoreBoard");
    let btnscore = document.getElementById("scorebutton");
    let credits = document.getElementById("allCredits");
    let btncredit = document.getElementById("creditbutton");

    if (game.style.display === "none") {
        game.style.display = "block";
        btnscore.textContent = 'SCOREBOARD';
        score.style.display = "none";
        btnscore.style.display = "block";
        btncredit.textContent = 'CREDITS';
        credits.style.display = "none";

    } else {
        game.style.display = "none";
        btnscore.style.display = "none";
        btncredit.textContent = 'BACK';
        btncredit.style.display = "block";
        credits.style.display = "block";
    }
}

function runningSound() {

    let runLoop = document.getElementById("runLoop");

    if (!runLoop.paused) {

        runLoop.pause();

    } else {
        runLoop.play();

    }

}

function music() {

    let sample = document.getElementById("music");
    let audioIcon = document.getElementById("audioIcon");

    if (!sample.paused) {

        sample.pause();
        audioIcon.src = "assets/webpage/music.png";

    } else {
        sample.playbackRate = 1.25;
        sample.play();
        audioIcon.src = "assets/webpage/mute.png";
    }
}

function play() {

    let button = document.getElementById("tutorial")
    let spawn = document.getElementById("spawn")
    let player = document.getElementById("player")
    let viking2 = document.getElementById("viking2")
    let score = document.getElementById("showScore")
    let pause = document.getElementById("pause")
    let audio = document.getElementById("audioIcon")
    let scoreButton = document.getElementById("scorebutton");
    let creditButton = document.getElementById("creditbutton");

    document.body.style.cursor = 'none';

    button.style.display = "none"
    viking2.style.display = "none"
    spawn.style.display = "block"
    player.style.display = "block"
    score.style.display = "block"
    pause.style.display = "block"
    audio.style.display = "block"
    scoreButton.style.display = "none";
    creditButton.style.display = "none";

    resetCounter()

    setTimeout(function () {
        ennemyRunning();
    }, 1000);

    var overlapEnnemy = false;
    var overlapCoin1 = false;
    var overlapCoin2 = false;
    var counter = 0


    let checkDead = setInterval(function () {

        let character = document.getElementById("player")
        let gameOver = document.getElementById("gameOver")
        let pause = document.getElementById("pause")
        let deadBjorn = document.getElementById("deadBjorn")
        let showScore = document.getElementById("showScore")
        let showCoins = document.getElementById("showCoins")
        let endScore = document.getElementById("endScore")
        let playerScore = document.getElementById("playerScore")

        let pig = document.getElementById("pig")
        let wolf = document.getElementById("wolf")
        let bird = document.getElementById("bird")
        let bird2 = document.getElementById("bird2")
        let coin1 = document.getElementById("coin1")
        let coin2 = document.getElementById("coin2")

        let hitboxPlayer = player.getBoundingClientRect()
        let hitboxPig = pig.getBoundingClientRect()
        let hitboxWolf = wolf.getBoundingClientRect()
        let hitboxBird = bird.getBoundingClientRect()
        let hitboxBird2 = bird2.getBoundingClientRect()
        let hitboxCoin1 = coin1.getBoundingClientRect()
        let hitboxCoin2 = coin2.getBoundingClientRect()

        //

        // COLLISION AVEC ENNEMIES
        if (hitboxPlayer.right - 50 >= hitboxPig.left && hitboxPlayer.bottom >= hitboxPig.top && hitboxPlayer.left <= hitboxPig.right - 50 ||
            hitboxPlayer.right - 50 >= hitboxWolf.left && hitboxPlayer.bottom >= hitboxWolf.top + 20 && hitboxPlayer.left <= hitboxWolf.right - 50 ||
            hitboxPlayer.right - 50 >= hitboxBird.left && hitboxPlayer.top <= hitboxBird.bottom && hitboxPlayer.left <= hitboxBird.right - 50  ||
            hitboxPlayer.right - 50 >= hitboxBird2.left && hitboxPlayer.top <= hitboxBird2.bottom && hitboxPlayer.left <= hitboxBird2.right - 50){
            overlapEnnemy = true;
        }

        // COLLISION AVEC PIECES
        if (hitboxPlayer.right - 50 >= hitboxCoin1.left && hitboxPlayer.bottom >= hitboxCoin1.top && hitboxPlayer.left <= hitboxCoin1.right - 30 ){
            overlapCoin1 = true;
        }

       if (hitboxPlayer.right - 50 >= hitboxCoin2.left && hitboxPlayer.top <= hitboxCoin2.bottom && hitboxPlayer.left <= hitboxCoin2.right - 30){
            overlapCoin2 = true;
        }

        if (overlapEnnemy) {
            document.body.style.cursor = 'pointer';
            endScore.innerHTML = score.innerHTML;
            playerScore.value = score.innerHTML;
            counter = 0;
            character.style.display = "none";
            pause.style.display = "none";
            score.style.display = "none";
            gameOver.style.display = "block";
            deadBjorn.src = deadBjorn.src.replace(/\?.*$/, "") + "?x=" + Math.random();
            deathSound.play();
            runningSound();
            clearInterval(checkDead);

        } else {
            if (!isPaused) {
                counter++;
                showScore.innerHTML = Math.floor(counter / 10);

                if (overlapCoin1) {

                    counter += 500
                    playGoldSound()
                    showCoins.style.display ="block"
                    coin1.style.display = "none"

                    setTimeout(function () {
                        coin1.style.display = "block"
                    }, 2000);

                    setTimeout(function () {
                        showCoins.style.display ="none"
                    }, 700);

                    overlapCoin1 = false

                }


                if (overlapCoin2) {

                    counter += 500
                    playGoldSound()
                    coin2.style.display = "none"
                    showCoins.style.display ="block"

                    setTimeout(function () {
                        coin2.style.display = "block"
                        showCoins.style.display ="none"
                    }, 2000);

                    setTimeout(function () {
                        showCoins.style.display ="none"
                    }, 700);

                    overlapCoin2 = false
                }


            }
        }

    }, 10);


}

function playAgain() {

    let button = document.getElementById("gameOver")
    let player = document.getElementById("player")
    let score = document.getElementById("showScore")
    let pause = document.getElementById("pause")

    button.style.display = "none";
    player.style.display = "block";
    score.style.display = "block";
    pause.style.display = "block";

    resetCounter();

}


function ennemyRunning() {
    let ennemies = document.getElementById("ennemies")
    let consumables = document.getElementById("consumables")
    ennemies.style.display = "block"
    consumables.style.display = "block"

    //On déclare les variables
    var pig = $('#pig')
    var wolf = $('#wolf')
    var bird = $('#bird')
    var bird2 = $('#bird2')
    var coin1 = $('#coin1')
    var coin2 = $('#coin2')

    // Une variable position qui sert à gérer le nombre de pixel par milliseconde
    let enemySpeed = 3.3;
    let coinSpeed = 2.5;

    // La limite à laquelle elle disparaît
    let xLimit = -200;


    // SPAWN RANDOMIZER
    let pigSpawn = [1500, 1500, 1500, 3700,  3700, 5200];
    let wolfSpawn = [2000, 2000, 2500, 4200, 4200, 5700];
    let birdSpawn = [2500, 2500, 3000, 4700, 4700, 6300];
    let bird2Spawn = [2700, 1700, 1700, 3400, 4600, 7000];

    let coinSpawn = [2000, 3000, 4000, 5000, 6000];
    let coin2Spawn = [1500, 2500, 3500, 4500, 5500];

    // La position initiale des images
    pig.offset({top: 559, left: pigSpawn[randomize()]})
    wolf.offset({top: 555, left: wolfSpawn[randomize()]})
    bird.offset({top: 390, left:  birdSpawn[randomize()]})
    bird2.offset({top: 400, left:  bird2Spawn[randomize()]})

    coin1.offset({top: 573, left: 1500})
    coin2.offset({top: 453, left: 3500})

    // Le loop de déplacement
    setInterval(function () {
        if (!isPaused) {

            let pig = $('#pig')
            let wolf = $('#wolf')
            let bird = $('#bird')
            let bird2 = $('#bird2')

            // La où elle réapparaît
            let randomPig = pigSpawn[randomize()]
            let randomWolf =  wolfSpawn[randomize()]
            let randomBird =  birdSpawn[randomize()]
            let randomBird2 =  bird2Spawn[randomize()]

            let randomCoin1 = coinSpawn[randomize()]
            let randomCoin2 = coin2Spawn[randomize()]

            let runPig = pig.offset().left;
            let runWolf = wolf.offset().left;
            let runBird = bird.offset().left;
            let runBird2 = bird2.offset().left;
            let runCoin1 = coin1.offset().left;
            let runCoin2 = coin2.offset().left;

            // La note augmente de 3px
            pig.offset({left: runPig - enemySpeed})
            wolf.offset({left: runWolf - enemySpeed})
            bird.offset({left: runBird - enemySpeed})
            bird2.offset({left: runBird2 - enemySpeed})
            coin1.offset({left: runCoin1 - coinSpeed})
            coin2.offset({left: runCoin2 - coinSpeed})

            // Si la position dépasse la limite alors
            if (pig.offset().left <= xLimit) {
                // Elle revient à la position aléatoire
                pig.offset({left: randomPig})
            }

            if (wolf.offset().left <= xLimit) {
                wolf.offset({left: randomWolf})
            }

            if (bird.offset().left <= xLimit) {
                bird.offset({left: randomBird})
            }

            if (bird2.offset().left <= xLimit) {
                bird2.offset({left: randomBird2})
            }

            if (coin1.offset().left <= xLimit) {
                coin1.offset({left: randomCoin1})
            }
            if (coin2.offset().left <= xLimit) {
                coin2.offset({left: randomCoin2})
            }

            overlapEnemy = checkEnemy();

            if(overlapEnemy === "pigwolf"){
                wolf.offset({left: randomWolf})
                overlapEnemy = false;
            }

            if(overlapEnemy === "birdwolf"){
                bird.offset({left: randomWolf})
                overlapEnemy = false;
            }

            if(overlapEnemy === "pigbird"){
                pig.offset({left: randomPig})
                overlapEnemy = false;
            }

            if(overlapEnemy === "birdbird2"){
                bird2.offset({left: randomBird2})
                overlapEnemy = false;
            }

            if(overlapEnemy === "pigbird2"){
                pig.offset({left: randomBird2})
                overlapEnemy = false;
            }

            if(overlapEnemy === "wolfbird2"){
                wolf.offset({left: randomBird2})
                overlapEnemy = false;
            }
        }
    })
}

 function checkEnemy(){
    // COLLISION ENNEMIES / ENNEMIES

    let pig = document.getElementById("pig")
    let wolf = document.getElementById("wolf")
    let bird = document.getElementById("bird")
    let bird2 = document.getElementById("bird2")

    let hitboxPig = pig.getBoundingClientRect()
    let hitboxWolf = wolf.getBoundingClientRect()
    let hitboxBird = bird.getBoundingClientRect()
    let hitboxBird2 = bird2.getBoundingClientRect()


    //COLLISION ENTRE ENNEMIES
    if (hitboxPig.right +250  >= hitboxWolf.left -250 &&  hitboxPig.left -250 <= hitboxWolf.right +250 && hitboxWolf.right  >= hitboxPig.left -250  &&  hitboxWolf.left -250 <= hitboxPig.right+250)
    {
        return "pigwolf"
    }

     if (hitboxBird.right +250  >= hitboxWolf.left -250 &&  hitboxBird.left -250 <= hitboxWolf.right +250 && hitboxWolf.right  >= hitboxBird.left -250  &&  hitboxWolf.left -250 <= hitboxBird.right+250)
     {
         return "birdwolf"
     }

     if (hitboxPig.right +250  >= hitboxBird.left -250 &&  hitboxPig.left -250 <= hitboxBird.right +250 && hitboxBird.right  >= hitboxPig.left -250  &&  hitboxBird.left -250 <= hitboxPig.right+250)
     {
         return "pigbird"
     }

     if (hitboxBird2.right +250  >= hitboxBird.left -250 &&  hitboxBird2.left -250 <= hitboxBird.right +250 && hitboxBird.right  >= hitboxBird2.left -250  &&  hitboxBird.left -250 <= hitboxBird2.right+250)
     {
         return "birdbird2"
     }

     if (hitboxPig.right +250  >= hitboxBird2.left -250 &&  hitboxPig.left -250 <= hitboxBird2.right +250 && hitboxBird2.right  >= hitboxPig.left -250  &&  hitboxBird2.left -250 <= hitboxPig.right+250)
     {
         return "pigbird2"
     }

     if (hitboxWolf.right +250  >= hitboxBird2.left -250 &&  hitboxWolf.left -250 <= hitboxBird2.right +250 && hitboxBird2.right  >= hitboxWolf.left -250  &&  hitboxBird2.left -250 <= hitboxWolf.right+250)
     {
         return "wolfbird2"
     }


 }


function pause() {
    let player = document.getElementById("player")
    let bg1 = document.getElementById("bg1")
    let bg2 = document.getElementById("bg2")
    let bg3 = document.getElementById("bg3")
    let ground = document.getElementById("ground")
    let score = document.getElementById("showScore")
    let pause = document.getElementById("pause")
    let pauseScreen = document.getElementById("pauseMenu")
    let pig = document.getElementById("pig")
    let wolf = document.getElementById("wolf")
    let bird = document.getElementById("bird")
    let bird2 = document.getElementById("bird2")
    let coin1 = document.getElementById("coin1")
    let coin2 = document.getElementById("coin2")


    if (!isPaused) {
        player.style.animationPlayState = "paused";
        player.style.backgroundImage = "url(assets/player/viking_run.png)"
        pig.style.backgroundImage = "url(assets/ennemies/pig.png)"
        wolf.style.backgroundImage = "url(assets/ennemies/wolf.png)"
        bird.style.backgroundImage = "url(assets/ennemies/bird.png)"
        bird2.style.backgroundImage = "url(assets/ennemies/bird2.png)"
        coin1.style.backgroundImage = "url(assets/consumable/coin.png"
        coin2.style.backgroundImage = "url(assets/consumable/coin.png"
        bg1.style.animationPlayState = "paused";
        bg2.style.animationPlayState = "paused";
        bg3.style.animationPlayState = "paused";
        ground.style.animationPlayState = "paused";
        pause.innerHTML = "▶";
        pause.style.fontSize = '100px';
        pause.classList.add('fe-pulse');
        pauseScreen.style.display = "block";
        runningSound();
        isPaused = true;

    } else {
        player.style.animationPlayState = "running";
        player.style.backgroundImage = "url(assets/player/viking_run.gif)"
        pig.style.backgroundImage = "url(assets/ennemies/pig.gif)"
        wolf.style.backgroundImage = "url(assets/ennemies/wolf.gif)"
        bird.style.backgroundImage = "url(assets/ennemies/bird.gif)"
        bird2.style.backgroundImage = "url(assets/ennemies/bird2.gif)"
        coin1.style.backgroundImage = "url(assets/consumable/coin.gif"
        coin2.style.backgroundImage = "url(assets/consumable/coin.gif"
        bg1.style.animationPlayState = "running";
        bg2.style.animationPlayState = "running";
        bg3.style.animationPlayState = "running";
        ground.style.animationPlayState = "running";

        pause.innerHTML = "II";
        pause.style.fontSize = '55px';
        pause.classList.remove('fe-pulse');
        pauseScreen.style.display = "none";
        runningSound();
        isPaused = false;
    }

}


function jump() {

    let player = document.getElementById("player");

    if (player.classList != "jump") {
        jumpSound.play();
        player.classList.add("jump");

        setTimeout(function () {
            player.classList.remove("jump");
        }, 700);
    }
}


function runningSound() {

    let runLoop = document.getElementById("runLoop");

    if (!runLoop.paused) {
        runLoop.pause();

    } else {
        runLoop.play();
    }
}

